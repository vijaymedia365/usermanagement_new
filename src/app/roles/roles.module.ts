import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";

import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { RolesRoutingModule } from "./roles-routing.module";
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatchHeightModule } from "../shared/directives/match-height.directive";
import { RolesComponent } from "./roles.component";

@NgModule({
    imports: [
        CommonModule,
        RolesRoutingModule,
        NgxChartsModule,
        NgbModule,
        MatchHeightModule,
        NgxDatatableModule
    ],
    declarations: [
        RolesComponent,
   ]
})
export class RolesModule { }
