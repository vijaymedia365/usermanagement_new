import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";

import { DashboardRoutingModule } from "./dashboard-routing.module";
import { ChartistModule } from 'ng-chartist';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { MatchHeightModule } from "../shared/directives/match-height.directive";

import { EcommerceComponent } from "./eCommerce/eCommerce.component";
import { AnalyticsComponent } from "./analytics/analytics.component";
import { ChartComponent } from "./chart/chart.component";


@NgModule({
    imports: [
        CommonModule,
        DashboardRoutingModule,
        ChartistModule,
        NgbModule,
        MatchHeightModule,
        NgxChartsModule
    ],
    exports: [],
    declarations: [
        EcommerceComponent,
        AnalyticsComponent,
        ChartComponent
    ],
    providers: [],
})
export class DashboardModule { }
