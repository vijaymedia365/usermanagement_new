import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.scss']
})

export class NavbarComponent {
    currentLang = 'en';
    toggleClass = 'ft-maximize';
    public isCollapsed = true;
    public isAdmin = true;
    
    constructor(public translate: TranslateService, private router: Router, private route: ActivatedRoute) {
        const browserLang: string = translate.getBrowserLang();
        translate.use(browserLang.match(/en|es|pt|de/) ? browserLang : 'en');
        let users = JSON.parse(localStorage.getItem('currentUser'));
        if (users && users['role_id'] != '4' && users['role_id'] != "1" && users['role_id'] != "2"){ //admin / super admin role id
          this.isAdmin = false;          
        }
    }

    ChangeLanguage(language: string) {
        this.translate.use(language);
    }

    logOut() {
        this.router.navigate(['/pages/login']);
        //this.router.navigate(['login'], { relativeTo: this.route.parent });
    }

    ToggleClass() {
        if (this.toggleClass === 'ft-maximize') {
            this.toggleClass = 'ft-minimize';
        }
        else
            this.toggleClass = 'ft-maximize'
    }
}
