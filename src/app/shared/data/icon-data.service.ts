export var iconData = [
    {
    "name": "ft-activity",
    "value": "ft-activity",
    },
    {
    "name": "ft-airplay",
    "value": "ft-airplay",
    },
    {
    "name": "ft-alert-circle",
    "value": "ft-alert-circle",
    },
    {
    "name": "ft-alert-octagon",
    "value": "ft-alert-octagon",
    },
    {
    "name": "ft-alert-triangle",
    "value": "ft-alert-triangle",
    },
    {
    "name": "ft-align-center",
    "value": "ft-align-center",
    },
    {
    "name": "ft-align-justify",
    "value": "ft-align-justify",
    },
    {
    "name": "ft-align-left",
    "value": "ft-align-left",
    },
    {
    "name": "ft-align-right",
    "value": "ft-align-right",
    },
    {
    "name": "ft-anchor",
    "value": "ft-anchor",
    },
    {
    "name": "ft-aperture",
    "value": "ft-aperture",
    },
    {
    "name": "ft-arrow-down-left",
    "value": "ft-arrow-down-left",
    },
    {
    "name": "ft-arrow-down-right",
    "value": "ft-arrow-down-right",
    },
    {
    "name": "ft-arrow-down",
    "value": "ft-arrow-down",
    },
    {
    "name": "ft-arrow-left",
    "value": "ft-arrow-left",
    },
    {
    "name": "ft-arrow-right",
    "value": "ft-arrow-right",
    },
    {
    "name": "ft-arrow-up-left",
    "value": "ft-arrow-up-left",
    },
    {
    "name": "ft-arrow-up-right",
    "value": "ft-arrow-up-right",
    },
    {
    "name": "ft-arrow-up",
    "value": "ft-arrow-up",
    },
    {
    "name": "ft-at-sign",
    "value": "ft-at-sign",
    },
    {
    "name": "ft-award",
    "value": "ft-award",
    },
    {
    "name": "ft-bar-chart-2",
    "value": "ft-bar-chart-2",
    },
    {
    "name": "ft-bar-chart",
    "value": "ft-bar-chart",
    },
    {
    "name": "ft-battery-charging",
    "value": "ft-battery-charging",
    },
    {
    "name": "ft-battery",
    "value": "ft-battery",
    },
    {
    "name": "ft-bell-off",
    "value": "ft-bell-off",
    },
    {
    "name": "ft-bell",
    "value": "ft-bell",
    },
    {
    "name": "ft-bluetooth",
    "value": "ft-bluetooth",
    },
    {
    "name": "ft-bold",
    "value": "ft-bold",
    },
    {
    "name": "ft-book",
    "value": "ft-book",
    },
    {
    "name": "ft-bookmark",
    "value": "ft-bookmark",
    },
    {
    "name": "ft-box",
    "value": "ft-box",
    },
    {
    "name": "ft-briefcase",
    "value": "ft-briefcase",
    },
    {
    "name": "ft-calendar",
    "value": "ft-calendar",
    },
    {
    "name": "ft-camera-off",
    "value": "ft-camera-off",
    },
    {
    "name": "ft-camera",
    "value": "ft-camera",
    },
    {
    "name": "ft-cast",
    "value": "ft-cast",
    },
    {
    "name": "ft-check-circle",
    "value": "ft-check-circle",
    },
    {
    "name": "ft-check-square",
    "value": "ft-check-square",
    },
    {
    "name": "ft-check",
    "value": "ft-check",
    },
    {
    "name": "ft-chevron-down",
    "value": "ft-chevron-down",
    },
    {
    "name": "ft-chevron-left",
    "value": "ft-chevron-left",
    },
    {
    "name": "ft-chevron-right",
    "value": "ft-chevron-right",
    },
    {
    "name": "ft-chevron-up",
    "value": "ft-chevron-up",
    },
    {
    "name": "ft-chevrons-down",
    "value": "ft-chevrons-down",
    },
    {
    "name": "ft-chevrons-left",
    "value": "ft-chevrons-left",
    },
    {
    "name": "ft-chevrons-right",
    "value": "ft-chevrons-right",
    },
    {
    "name": "ft-chevrons-up",
    "value": "ft-chevrons-up",
    },
    {
    "name": "ft-chrome",
    "value": "ft-chrome",
    },
    {
    "name": "ft-circle",
    "value": "ft-circle",
    },
    {
    "name": "ft-clipboard",
    "value": "ft-clipboard",
    },
    {
    "name": "ft-clock",
    "value": "ft-clock",
    },
    {
    "name": "ft-cloud-drizzle",
    "value": "ft-cloud-drizzle",
    },
    {
    "name": "ft-cloud-lightning",
    "value": "ft-cloud-lightning",
    },
    {
    "name": "ft-cloud-off",
    "value": "ft-cloud-off",
    },
    {
    "name": "ft-cloud-rain",
    "value": "ft-cloud-rain",
    },
    {
    "name": "ft-cloud-snow",
    "value": "ft-cloud-snow",
    },
    {
    "name": "ft-cloud",
    "value": "ft-cloud",
    },
    {
    "name": "ft-codepen",
    "value": "ft-codepen",
    },
    {
    "name": "ft-command",
    "value": "ft-command",
    },
    {
    "name": "ft-compass",
    "value": "ft-compass",
    },
    {
    "name": "ft-copy",
    "value": "ft-copy",
    },
    {
    "name": "ft-corner-down-left",
    "value": "ft-corner-down-left",
    },
    {
    "name": "ft-corner-down-right",
    "value": "ft-corner-down-right",
    },
    {
    "name": "ft-corner-left-down",
    "value": "ft-corner-left-down",
    },
    {
    "name": "ft-corner-left-up",
    "value": "ft-corner-left-up",
    },
    {
    "name": "ft-corner-right-down",
    "value": "ft-corner-right-down",
    },
    {
    "name": "ft-corner-right-up",
    "value": "ft-corner-right-up",
    },
    {
    "name": "ft-corner-up-left",
    "value": "ft-corner-up-left",
    },
    {
    "name": "ft-corner-up-right",
    "value": "ft-corner-up-right",
    },
    {
    "name": "ft-cpu",
    "value": "ft-cpu",
    },
    {
    "name": "ft-credit-card",
    "value": "ft-credit-card",
    },
    {
    "name": "ft-crop",
    "value": "ft-crop",
    },
    {
    "name": "ft-crosshair",
    "value": "ft-crosshair",
    },
    {
    "name": "ft-delete",
    "value": "ft-delete",
    },
    {
    "name": "ft-disc",
    "value": "ft-disc",
    },
    {
    "name": "ft-download-cloud",
    "value": "ft-download-cloud",
    },
    {
    "name": "ft-download",
    "value": "ft-download",
    },
    {
    "name": "ft-droplet",
    "value": "ft-droplet",
    },
    {
    "name": "ft-edit-2",
    "value": "ft-edit-2",
    },
    {
    "name": "ft-edit-3",
    "value": "ft-edit-3",
    },
    {
    "name": "ft-edit",
    "value": "ft-edit",
    },
    {
    "name": "ft-external-link",
    "value": "ft-external-link",
    },
    {
    "name": "ft-eye-off",
    "value": "ft-eye-off",
    },
    {
    "name": "ft-eye",
    "value": "ft-eye",
    },
    {
    "name": "ft-facebook",
    "value": "ft-facebook",
    },
    {
    "name": "ft-fast-forward",
    "value": "ft-fast-forward",
    },
    {
    "name": "ft-feather",
    "value": "ft-feather",
    },
    {
    "name": "ft-file-minus",
    "value": "ft-file-minus",
    },
    {
    "name": "ft-file-plus",
    "value": "ft-file-plus",
    },
    {
    "name": "ft-file-text",
    "value": "ft-file-text",
    },
    {
    "name": "ft-file",
    "value": "ft-file",
    },
    {
    "name": "ft-film",
    "value": "ft-film",
    },
    {
    "name": "ft-filter",
    "value": "ft-filter",
    },
    {
    "name": "ft-flag",
    "value": "ft-flag",
    },
    {
    "name": "ft-folder",
    "value": "ft-folder",
    },
    {
    "name": "ft-github",
    "value": "ft-github",
    },
    {
    "name": "ft-gitlab",
    "value": "ft-gitlab",
    },
    {
    "name": "ft-globe",
    "value": "ft-globe",
    },
    {
    "name": "ft-grid",
    "value": "ft-grid",
    },
    {
    "name": "ft-hash",
    "value": "ft-hash",
    },
    {
    "name": "ft-headphones",
    "value": "ft-headphones",
    },
    {
    "name": "ft-heart",
    "value": "ft-heart",
    },
    {
    "name": "ft-heart",
    "value": "ft-heart",
    },
    {
    "name": "ft-help-circle",
    "value": "ft-help-circle",
    },
    {
    "name": "ft-image",
    "value": "ft-image",
    },
    {
    "name": "ft-inbox",
    "value": "ft-inbox",
    },
    {
    "name": "ft-info",
    "value": "ft-info",
    },
    {
    "name": "ft-instagram",
    "value": "ft-instagram",
    },
    {
    "name": "ft-italic",
    "value": "ft-italic",
    },
    {
    "name": "ft-layers",
    "value": "ft-layers",
    },
    {
    "name": "ft-layout",
    "value": "ft-layout",
    },
    {
    "name": "ft-life-buoy",
    "value": "ft-life-buoy",
    },
    {
    "name": "ft-link-2",
    "value": "ft-link-2",
    },
    {
    "name": "ft-link",
    "value": "ft-link",
    },
    {
    "name": "ft-list",
    "value": "ft-list",
    },
    {
    "name": "ft-loader",
    "value": "ft-loader",
    },
    {
    "name": "ft-lock",
    "value": "ft-lock",
    },
    {
    "name": "ft-log-in",
    "value": "ft-log-in",
    },
    {
    "name": "ft-log-out",
    "value": "ft-log-out",
    },
    {
    "name": "ft-mail",
    "value": "ft-mail",
    },
    {
    "name": "ft-map-pin",
    "value": "ft-map-pin",
    },
    {
    "name": "ft-map",
    "value": "ft-map",
    },
    {
    "name": "ft-maximize-2",
    "value": "ft-maximize-2",
    },
    {
    "name": "ft-maximize",
    "value": "ft-maximize",
    },
    {
    "name": "ft-menu",
    "value": "ft-menu",
    },
    {
    "name": "ft-message-circle",
    "value": "ft-message-circle",
    },
    {
    "name": "ft-message-square",
    "value": "ft-message-square",
    },
    {
    "name": "ft-mic-off",
    "value": "ft-mic-off",
    },
    {
    "name": "ft-mic",
    "value": "ft-mic",
    },
    {
    "name": "ft-minimize-2",
    "value": "ft-minimize-2",
    },
    {
    "name": "ft-minimize",
    "value": "ft-minimize",
    },
    {
    "name": "ft-minus-circle",
    "value": "ft-minus-circle",
    },
    {
    "name": "ft-minus-square",
    "value": "ft-minus-square",
    },
    {
    "name": "ft-minus",
    "value": "ft-minus",
    },
    {
    "name": "ft-monitor",
    "value": "ft-monitor",
    },
    {
    "name": "ft-moon",
    "value": "ft-moon",
    },
    {
    "name": "ft-more-horizontal",
    "value": "ft-more-horizontal",
    },
    {
    "name": "ft-more-vertical",
    "value": "ft-more-vertical",
    },
    {
    "name": "ft-move",
    "value": "ft-move",
    },
    {
    "name": "ft-music",
    "value": "ft-music",
    },
    {
    "name": "ft-navigation-2",
    "value": "ft-navigation-2",
    },
    {
    "name": "ft-navigation",
    "value": "ft-navigation",
    },
    {
    "name": "ft-octagon",
    "value": "ft-octagon",
    },
    {
    "name": "ft-package",
    "value": "ft-package",
    },
    {
    "name": "ft-paperclip",
    "value": "ft-paperclip",
    },
    {
    "name": "ft-pause-circle",
    "value": "ft-pause-circle",
    },
    {
    "name": "ft-pause",
    "value": "ft-pause",
    },
    {
    "name": "ft-percent",
    "value": "ft-percent",
    },
    {
    "name": "ft-phone-call",
    "value": "ft-phone-call",
    },
    {
    "name": "ft-phone-forwarded",
    "value": "ft-phone-forwarded",
    },
    {
    "name": "ft-phone-incoming",
    "value": "ft-phone-incoming",
    },
    {
    "name": "ft-phone-missed",
    "value": "ft-phone-missed",
    },
    {
    "name": "ft-phone-off",
    "value": "ft-phone-off",
    },
    {
    "name": "ft-phone-outgoing",
    "value": "ft-phone-outgoing",
    },
    {
    "name": "ft-phone",
    "value": "ft-phone",
    },
    {
    "name": "ft-pie-chart",
    "value": "ft-pie-chart",
    },
    {
    "name": "ft-play-circle",
    "value": "ft-play-circle",
    },
    {
    "name": "ft-play",
    "value": "ft-play",
    },
    {
    "name": "ft-plus-circle",
    "value": "ft-plus-circle",
    },
    {
    "name": "ft-plus-square",
    "value": "ft-plus-square",
    },
    {
    "name": "ft-plus",
    "value": "ft-plus",
    },
    {
    "name": "ft-pocket",
    "value": "ft-pocket",
    },
    {
    "name": "ft-power",
    "value": "ft-power",
    },
    {
    "name": "ft-printer",
    "value": "ft-printer",
    },
    {
    "name": "ft-radio",
    "value": "ft-radio",
    },
    {
    "name": "ft-refresh-ccw",
    "value": "ft-refresh-ccw",
    },
    {
    "name": "ft-refresh-cw",
    "value": "ft-refresh-cw",
    },
    {
    "name": "ft-repeat",
    "value": "ft-repeat",
    },
    {
    "name": "ft-rewind",
    "value": "ft-rewind",
    },
    {
    "name": "ft-rotate-ccw",
    "value": "ft-rotate-ccw",
    },
    {
    "name": "ft-rotate-cw",
    "value": "ft-rotate-cw",
    },
    {
    "name": "ft-save",
    "value": "ft-save",
    },
    {
    "name": "ft-scissors",
    "value": "ft-scissors",
    },
    {
    "name": "ft-search",
    "value": "ft-search",
    },
    {
    "name": "ft-server",
    "value": "ft-server",
    },
    {
    "name": "ft-settings",
    "value": "ft-settings",
    },
    {
    "name": "ft-share-2",
    "value": "ft-share-2",
    },
    {
    "name": "ft-share",
    "value": "ft-share",
    },
    {
    "name": "ft-shield",
    "value": "ft-shield",
    },
    {
    "name": "ft-shopping-cart",
    "value": "ft-shopping-cart",
    },
    {
    "name": "ft-shuffle",
    "value": "ft-shuffle",
    },
    {
    "name": "ft-sidebar",
    "value": "ft-sidebar",
    },
    {
    "name": "ft-skip-back",
    "value": "ft-skip-back",
    },
    {
    "name": "ft-skip-forward",
    "value": "ft-skip-forward",
    },
    {
    "name": "ft-slack",
    "value": "ft-slack",
    },
    {
    "name": "ft-slash",
    "value": "ft-slash",
    },
    {
    "name": "ft-sliders",
    "value": "ft-sliders",
    },
    {
    "name": "ft-smartphone",
    "value": "ft-smartphone",
    },
    {
    "name": "ft-speaker",
    "value": "ft-speaker",
    },
    {
    "name": "ft-square",
    "value": "ft-square",
    },
    {
    "name": "ft-star",
    "value": "ft-star",
    },
    {
    "name": "ft-stop-circle",
    "value": "ft-stop-circle",
    },
    {
    "name": "ft-sun",
    "value": "ft-sun",
    },
    {
    "name": "ft-sunrise",
    "value": "ft-sunrise",
    },
    {
    "name": "ft-sunset",
    "value": "ft-sunset",
    },
    {
    "name": "ft-tablet",
    "value": "ft-tablet",
    },
    {
    "name": "ft-tag",
    "value": "ft-tag",
    },
    {
    "name": "ft-target",
    "value": "ft-target",
    },
    {
    "name": "ft-thermometer",
    "value": "ft-thermometer",
    },
    {
    "name": "ft-thumbs-down",
    "value": "ft-thumbs-down",
    },
    {
    "name": "ft-thumbs-up",
    "value": "ft-thumbs-up",
    },
    {
    "name": "ft-toggle-left",
    "value": "ft-toggle-left",
    },
    {
    "name": "ft-toggle-right",
    "value": "ft-toggle-right",
    },
    {
    "name": "ft-trash-2",
    "value": "ft-trash-2",
    },
    {
    "name": "ft-trash",
    "value": "ft-trash",
    },
    {
    "name": "ft-trending-down",
    "value": "ft-trending-down",
    },
    {
    "name": "ft-trending-up",
    "value": "ft-trending-up",
    },
    {
    "name": "ft-triangle",
    "value": "ft-triangle",
    },
    {
    "name": "ft-tv",
    "value": "ft-tv",
    },
    {
    "name": "ft-twitter",
    "value": "ft-twitter",
    },
    {
    "name": "ft-type",
    "value": "ft-type",
    },
    {
    "name": "ft-umbrella",
    "value": "ft-umbrella",
    },
    {
    "name": "ft-underline",
    "value": "ft-underline",
    },
    {
    "name": "ft-unlock",
    "value": "ft-unlock",
    },
    {
    "name": "ft-upload-cloud",
    "value": "ft-upload-cloud",
    },
    {
    "name": "ft-upload",
    "value": "ft-upload",
    },
    {
    "name": "ft-user-check",
    "value": "ft-user-check",
    },
    {
    "name": "ft-user-minus",
    "value": "ft-user-minus",
    },
    {
    "name": "ft-user-plus",
    "value": "ft-user-plus",
    },
    {
    "name": "ft-user-x",
    "value": "ft-user-x",
    },
    {
    "name": "ft-user",
    "value": "ft-user",
    },
    {
    "name": "ft-users",
    "value": "ft-users",
    },
    {
    "name": "ft-video-off",
    "value": "ft-video-off",
    },
    {
    "name": "ft-video",
    "value": "ft-video",
    },
    {
    "name": "ft-voicemail",
    "value": "ft-voicemail",
    },
    {
    "name": "ft-volume-1",
    "value": "ft-volume-1",
    },
    {
    "name": "ft-volume-2",
    "value": "ft-volume-2",
    },
    {
    "name": "ft-volume-x",
    "value": "ft-volume-x",
    },
    {
    "name": "ft-volume",
    "value": "ft-volume",
    },
    {
    "name": "ft-watch",
    "value": "ft-watch",
    },
    {
    "name": "ft-wifi-off",
    "value": "ft-wifi-off",
    },
    {
    "name": "ft-wifi",
    "value": "ft-wifi",
    },
    {
    "name": "ft-wind",
    "value": "ft-wind",
    },
    {
    "name": "ft-x-circle",
    "value": "ft-x-circle",
    },
    {
    "name": "ft-x-square",
    "value": "ft-x-square",
    },
    {
    "name": "ft-x",
    "value": "ft-x",
    },
    {
    "name": "ft-zap",
    "value": "ft-zap",
    },
    {
    "name": "ft-zoom-in",
    "value": "ft-zoom-in",
    },
    {
    "name": "ft-zoom-out",
    "value": "ft-zoom-out",
    }
];

export var colorPalette = [
  {
  	"name": "Primary",
  	"value": "#666EE8"
  },
  {
  	"name": "Success",
  	"value": "#28D094"
  },
  {
  	"name": "Danger",
  	"value": "#FF4961"
  },
  {
  	"name": "Info",
  	"value": "#1E9FF2"
  },
  {
  	"name": "Warning",
  	"value": "#FF9149"
  },
  {
  	"name": "Red",
  	"value": "#f44336"
  },
  {
  	"name": "Pink",
  	"value": "#e91e63"
  },
  {
  	"name": "Purple",
  	"value": "#9c27b0"
  },
  {
  	"name": "Deep Purple",
  	"value": "#673ab7"
  },
  {
  	"name": "Indigo",
  	"value": "#3f51b5"
  },
  {
  	"name": "Blue",
  	"value": "#2196f3"
  },
  {
  	"name": "Light Blue",
  	"value": "#03a9f4"
  },
  {
  	"name": "Cyan",
  	"value": "#00bcd4"
  },
  {
  	"name": "Teal",
  	"value": "#009688"
  },
  {
  	"name": "Green",
  	"value": "#4caf50"
  },
  {
  	"name": "Light Green",
  	"value": "#8bc34a"
  },
  {
  	"name": "Lime",
  	"value": "#cddc39"
  },
  {
  	"name": "Yellow",
  	"value": "#ffeb3b"
  },
  {
  	"name": "Amber",
  	"value": "#ffc107"
  },
  {
  	"name": "Orange",
  	"value": "#ff9800"
  },
  {
  	"name": "Deep Orange",
  	"value": "#ff5722"
  },
  {
  	"name": "Brown",
  	"value": "#795548"
  },
  {
  	"name": "Grey",
  	"value": "#9e9e9e"
  },
  {
  	"name": "Blue Grey",
  	"value": "#607d8b"
  }
]

export var simpleIconData = [
  {
  "name": "icon-user-female",
  "value": "icon-user-female"
  },
  {
  "name": "icon-user-follow",
  "value": "icon-user-follow"
  },
  {
  "name": "icon-user-following",
  "value": "icon-user-following"
  },
  {
  "name": "icon-user-unfollow",
  "value": "icon-user-unfollow"
  },
  {
  "name": "icon-trophy",
  "value": "icon-trophy"
  },
  {
  "name": "icon-screen-smartphone",
  "value": "icon-screen-smartphone"
  },
  {
  "name": "icon-screen-desktop",
  "value": "icon-screen-desktop"
  },
  {
  "name": "icon-plane",
  "value": "icon-plane"
  },
  {
  "name": "icon-notebook",
  "value": "icon-notebook"
  },
  {
  "name": "icon-moustache",
  "value": "icon-moustache"
  },
  {
  "name": "icon-mouse",
  "value": "icon-mouse"
  },
  {
  "name": "icon-magnet",
  "value": "icon-magnet"
  },
  {
  "name": "icon-energy",
  "value": "icon-energy"
  },
  {
  "name": "icon-emoticon-smile",
  "value": "icon-emoticon-smile"
  },
  {
  "name": "icon-disc",
  "value": "icon-disc"
  },
  {
  "name": "icon-cursor-move",
  "value": "icon-cursor-move"
  },
  {
  "name": "icon-crop",
  "value": "icon-crop"
  },
  {
  "name": "icon-credit-card",
  "value": "icon-credit-card"
  },
  {
  "name": "icon-chemistry",
  "value": "icon-chemistry"
  },
  {
  "name": "icon-user",
  "value": "icon-user"
  },
  {
  "name": "icon-speedometer",
  "value": "icon-speedometer"
  },
  {
  "name": "icon-social-youtube",
  "value": "icon-social-youtube"
  },
  {
  "name": "icon-social-twitter",
  "value": "icon-social-twitter"
  },
  {
  "name": "icon-social-tumblr",
  "value": "icon-social-tumblr"
  },
  {
  "name": "icon-social-facebook",
  "value": "icon-social-facebook"
  },
  {
  "name": "icon-social-dropbox",
  "value": "icon-social-dropbox"
  },
  {
  "name": "icon-social-dribbble",
  "value": "icon-social-dribbble"
  },
  {
  "name": "icon-shield",
  "value": "icon-shield"
  },
  {
  "name": "icon-screen-tablet",
  "value": "icon-screen-tablet"
  },
  {
  "name": "icon-magic-wand",
  "value": "icon-magic-wand"
  },
  {
  "name": "icon-hourglass",
  "value": "icon-hourglass"
  },
  {
  "name": "icon-graduation",
  "value": "icon-graduation"
  },
  {
  "name": "icon-ghost",
  "value": "icon-ghost"
  },
  {
  "name": "icon-game-controller",
  "value": "icon-game-controller"
  },
  {
  "name": "icon-fire",
  "value": "icon-fire"
  },
  {
  "name": "icon-eyeglasses",
  "value": "icon-eyeglasses"
  },
  {
  "name": "icon-envelope-open",
  "value": "icon-envelope-open"
  },
  {
  "name": "icon-envelope-letter",
  "value": "icon-envelope-letter"
  },
  {
  "name": "icon-bell",
  "value": "icon-bell"
  },
  {
  "name": "icon-badge",
  "value": "icon-badge"
  },
  {
  "name": "icon-anchor",
  "value": "icon-anchor"
  },
  {
  "name": "icon-wallet",
  "value": "icon-wallet"
  },
  {
  "name": "icon-vector",
  "value": "icon-vector"
  },
  {
  "name": "icon-speech",
  "value": "icon-speech"
  },
  {
  "name": "icon-puzzle",
  "value": "icon-puzzle"
  },
  {
  "name": "icon-printer",
  "value": "icon-printer"
  },
  {
  "name": "icon-present",
  "value": "icon-present"
  },
  {
  "name": "icon-playlist",
  "value": "icon-playlist"
  },
  {
  "name": "icon-pin",
  "value": "icon-pin"
  },
  {
  "name": "icon-picture",
  "value": "icon-picture"
  },
  {
  "name": "icon-map",
  "value": "icon-map"
  },
  {
  "name": "icon-layers",
  "value": "icon-layers"
  },
  {
  "name": "icon-handbag",
  "value": "icon-handbag"
  },
  {
  "name": "icon-globe-alt",
  "value": "icon-globe-alt"
  },
  {
  "name": "icon-globe",
  "value": "icon-globe"
  },
  {
  "name": "icon-frame",
  "value": "icon-frame"
  },
  {
  "name": "icon-folder-alt",
  "value": "icon-folder-alt"
  },
  {
  "name": "icon-film",
  "value": "icon-film"
  },
  {
  "name": "icon-feed",
  "value": "icon-feed"
  },
  {
  "name": "icon-earphones-alt",
  "value": "icon-earphones-alt"
  },
  {
  "name": "icon-earphones",
  "value": "icon-earphones"
  },
  {
  "name": "icon-drop",
  "value": "icon-drop"
  },
  {
  "name": "icon-drawer",
  "value": "icon-drawer"
  },
  {
  "name": "icon-docs",
  "value": "icon-docs"
  },
  {
  "name": "icon-directions",
  "value": "icon-directions"
  },
  {
  "name": "icon-direction",
  "value": "icon-direction"
  },
  {
  "name": "icon-diamond",
  "value": "icon-diamond"
  },
  {
  "name": "icon-cup",
  "value": "icon-cup"
  },
  {
  "name": "icon-compass",
  "value": "icon-compass"
  },
  {
  "name": "icon-call-out",
  "value": "icon-call-out"
  },
  {
  "name": "icon-call-in",
  "value": "icon-call-in"
  },
  {
  "name": "icon-call-end",
  "value": "icon-call-end"
  },
  {
  "name": "icon-calculator",
  "value": "icon-calculator"
  },
  {
  "name": "icon-bubbles",
  "value": "icon-bubbles"
  },
  {
  "name": "icon-briefcase",
  "value": "icon-briefcase"
  },
  {
  "name": "icon-book-open",
  "value": "icon-book-open"
  },
  {
  "name": "icon-basket-loaded",
  "value": "icon-basket-loaded"
  },
  {
  "name": "icon-basket",
  "value": "icon-basket"
  },
  {
  "name": "icon-bag",
  "value": "icon-bag"
  },
  {
  "name": "icon-action-undo",
  "value": "icon-action-undo"
  },
  {
  "name": "icon-action-redo",
  "value": "icon-action-redo"
  },
  {
  "name": "icon-wrench",
  "value": "icon-wrench"
  },
  {
  "name": "icon-umbrella",
  "value": "icon-umbrella"
  },
  {
  "name": "icon-trash",
  "value": "icon-trash"
  },
  {
  "name": "icon-tag",
  "value": "icon-tag"
  },
  {
  "name": "icon-support",
  "value": "icon-support"
  },
  {
  "name": "icon-size-fullscreen",
  "value": "icon-size-fullscreen"
  },
  {
  "name": "icon-size-actual",
  "value": "icon-size-actual"
  },
  {
  "name": "icon-shuffle",
  "value": "icon-shuffle"
  },
  {
  "name": "icon-share-alt",
  "value": "icon-share-alt"
  },
  {
  "name": "icon-share",
  "value": "icon-share"
  },
  {
  "name": "icon-rocket",
  "value": "icon-rocket"
  },
  {
  "name": "icon-question",
  "value": "icon-question"
  },
  {
  "name": "icon-pie-chart",
  "value": "icon-pie-chart"
  },
  {
  "name": "icon-pencil",
  "value": "icon-pencil"
  },
  {
  "name": "icon-note",
  "value": "icon-note"
  },
  {
  "name": "icon-music-tone-alt",
  "value": "icon-music-tone-alt"
  },
  {
  "name": "icon-music-tone",
  "value": "icon-music-tone"
  },
  {
  "name": "icon-microphone",
  "value": "icon-microphone"
  },
  {
  "name": "icon-loop",
  "value": "icon-loop"
  },
  {
  "name": "icon-logout",
  "value": "icon-logout"
  },
  {
  "name": "icon-login",
  "value": "icon-login"
  },
  {
  "name": "icon-list",
  "value": "icon-list"
  },
  {
  "name": "icon-like",
  "value": "icon-like"
  },
  {
  "name": "icon-home",
  "value": "icon-home"
  },
  {
  "name": "icon-grid",
  "value": "icon-grid"
  },
  {
  "name": "icon-graph",
  "value": "icon-graph"
  },
  {
  "name": "icon-equalizer",
  "value": "icon-equalizer"
  },
  {
  "name": "icon-dislike",
  "value": "icon-dislike"
  },
  {
  "name": "icon-cursor",
  "value": "icon-cursor"
  },
  {
  "name": "icon-control-start",
  "value": "icon-control-start"
  },
  {
  "name": "icon-control-rewind",
  "value": "icon-control-rewind"
  },
  {
  "name": "icon-control-play",
  "value": "icon-control-play"
  },
  {
  "name": "icon-control-pause",
  "value": "icon-control-pause"
  },
  {
  "name": "icon-control-forward",
  "value": "icon-control-forward"
  },
  {
  "name": "icon-control-end",
  "value": "icon-control-end"
  },
  {
  "name": "icon-calendar",
  "value": "icon-calendar"
  },
  {
  "name": "icon-bulb",
  "value": "icon-bulb"
  },
  {
  "name": "icon-bar-chart",
  "value": "icon-bar-chart"
  },
  {
  "name": "icon-arrow-up",
  "value": "icon-arrow-up"
  },
  {
  "name": "icon-arrow-right",
  "value": "icon-arrow-right"
  },
  {
  "name": "icon-arrow-left",
  "value": "icon-arrow-left"
  },
  {
  "name": "icon-arrow-down",
  "value": "icon-arrow-down"
  },
  {
  "name": "icon-ban",
  "value": "icon-ban"
  },
  {
  "name": "icon-bubble",
  "value": "icon-bubble"
  },
  {
  "name": "icon-camcorder",
  "value": "icon-camcorder"
  },
  {
  "name": "icon-camera",
  "value": "icon-camera"
  },
  {
  "name": "icon-check",
  "value": "icon-check"
  },
  {
  "name": "icon-clock",
  "value": "icon-clock"
  },
  {
  "name": "icon-close",
  "value": "icon-close"
  },
  {
  "name": "icon-cloud-download",
  "value": "icon-cloud-download"
  },
  {
  "name": "icon-cloud-upload",
  "value": "icon-cloud-upload"
  },
  {
  "name": "icon-doc",
  "value": "icon-doc"
  },
  {
  "name": "icon-envelope",
  "value": "icon-envelope"
  },
  {
  "name": "icon-eye",
  "value": "icon-eye"
  },
  {
  "name": "icon-flag",
  "value": "icon-flag"
  },
  {
  "name": "icon-folder",
  "value": "icon-folder"
  },
  {
  "name": "icon-heart",
  "value": "icon-heart"
  },
  {
  "name": "icon-info",
  "value": "icon-info"
  },
  {
  "name": "icon-key",
  "value": "icon-key"
  },
  {
  "name": "icon-link",
  "value": "icon-link"
  },
  {
  "name": "icon-lock",
  "value": "icon-lock"
  },
  {
  "name": "icon-lock-open",
  "value": "icon-lock-open"
  },
  {
  "name": "icon-magnifier",
  "value": "icon-magnifier"
  },
  {
  "name": "icon-magnifier-add",
  "value": "icon-magnifier-add"
  },
  {
  "name": "icon-magnifier-remove",
  "value": "icon-magnifier-remove"
  },
  {
  "name": "icon-paper-clip",
  "value": "icon-paper-clip"
  },
  {
  "name": "icon-paper-plane",
  "value": "icon-paper-plane"
  },
  {
  "name": "icon-plus",
  "value": "icon-plus"
  },
  {
  "name": "icon-pointer",
  "value": "icon-pointer"
  },
  {
  "name": "icon-power",
  "value": "icon-power"
  },
  {
  "name": "icon-refresh",
  "value": "icon-refresh"
  },
  {
  "name": "icon-reload",
  "value": "icon-reload"
  },
  {
  "name": "icon-settings",
  "value": "icon-settings"
  },
  {
  "name": "icon-star",
  "value": "icon-star"
  },
  {
  "name": "icon-symbol-female",
  "value": "icon-symbol-female"
  },
  {
  "name": "icon-symbol-male",
  "value": "icon-symbol-male"
  },
  {
  "name": "icon-target",
  "value": "icon-target"
  },
  {
  "name": "icon-volume-1",
  "value": "icon-volume-1"
  },
  {
  "name": "icon-volume-2",
  "value": "icon-volume-2"
  },
  {
  "name": "icon-volume-off",
  "value": "icon-volume-off"
  },
  {
  "name": "icon-users",
  "value": "icon-users"
  },
]
