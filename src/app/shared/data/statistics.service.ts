import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()
export class StatisticsService {
    constructor(private http: HttpClient) { }

    public timestamp = new Date().getTime();

    getclickCounts(id) {
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/getStats.php?action=links&q='+this.timestamp, { id })
            .subscribe(stats => {
                console.log(stats);               
                resolve(stats);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }

    getreports(){
        return new Promise(resolve => {
        this.http.get('https://click365.com.au/usermanagement/getStats.php?action=reports&q='+this.timestamp)
            .subscribe(reports => {
                console.log(reports);               
                resolve(reports);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }

    getReportById(id){
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/getStats.php?action=reportbyid&q='+this.timestamp, { id })
            .subscribe(reports => {
                console.log(reports);               
                resolve(reports);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }

    getEmailDomain(id){
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/getStats.php?action=emaildomain&q='+this.timestamp, { id })
            .subscribe(reports => {
                console.log(reports);               
                resolve(reports);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }

    gettimeline(id){
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/getStats.php?action=timeline&q='+this.timestamp, { id })
            .subscribe(reports => {
                console.log(reports);               
                resolve(reports);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }

    getLatestReport(){
        return new Promise(resolve => {
        this.http.get('https://click365.com.au/usermanagement/getStats.php?action=latestreport&q='+this.timestamp)
            .subscribe(reports => {
                console.log(reports);               
                resolve(reports);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }

    getopenclickcount(id){
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/getStats.php?action=openclickcount&q='+this.timestamp, { id })
            .subscribe(reports => {
                console.log(reports);               
                resolve(reports);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
}