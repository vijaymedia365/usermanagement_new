import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators, NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from "@angular/router";
import { GooglePlaceDirective } from "ngx-google-places-autocomplete";

import { UserService } from '../../shared/data/user.service';
import { ListsService } from '../../shared/data/lists.service';
import { OrganizationService } from '../../shared/data/organization.service';

@Component({
  selector: 'app-add-users',
  templateUrl: './add-users.component.html',
  styleUrls: ['./add-users.component.scss']
})
export class AddUsersComponent implements OnInit {
  @ViewChild('f') floatingLabelForm: NgForm;
  @ViewChild('vform') validationForm: FormGroup;
  @ViewChild("placesRef") placesRef : GooglePlaceDirective;
  regularForm: FormGroup;
  newaddress;
  listadmin;
  usernameerror = false;
  emailerror = false;
  showlistadmin;
  options = {
    types: ['geocode'],
    componentRestrictions: { country: 'AU' }
  }
  listDetails;
  submitted = false;
  public roles;
  public organizations;
  public isSuperAdmin: boolean = false;

  constructor(public userservice: UserService, public organizationservice: OrganizationService, private router: Router, private route: ActivatedRoute, private listsservice: ListsService) {
    let users = JSON.parse(localStorage.getItem('currentUser'));
    if(users){
      if(users['role_id'] == '2'){
        this.isSuperAdmin = true;
      }
    }
  }

  ngOnInit() {
    this.regularForm = new FormGroup({
      'username': new FormControl(null, [Validators.required]),
      'firstName': new FormControl(null, [Validators.required]),
      'email': new FormControl(null, [Validators.required, Validators.email]),
      'dob': new FormControl(null, [Validators.required]),
      'password': new FormControl(null, [Validators.required, Validators.minLength(4), Validators.maxLength(24)]),
      'lastName': new FormControl(null, [Validators.required]),
      'genderRadios': new FormControl('Female'),
      'address': new FormControl(null, [Validators.required]),
      'phone': new FormControl(null, [Validators.required]),
      'sublist': new FormControl(null, [Validators.required]),
      'role': new FormControl(null, [Validators.required]),
      'organization': new FormControl(null, [Validators.required])
        // 'inputEmail': new FormControl(null, [Validators.required, Validators.email]),
        // 'password': new FormControl(null, [Validators.required, Validators.minLength(4), Validators.maxLength(24)]),
        // 'textArea': new FormControl(null, [Validators.required]),
        // 'radioOption': new FormControl('Option one is this')
    }, {updateOn: 'blur'});
    	this.listsservice.getlist().then(data => {
	        console.log(data['lists']);
	        this.listDetails = data['lists'];
	    },
	    error => {
	    });
        this.userservice.getGroups().then(data => {
          this.roles = data['groups'];
        });
        this.organizationservice.getOrganization(new Date().getTime()).then(data => {
          this.organizations = data['organization'];
        });
	    if(localStorage.getItem('listadmin') == '1'){
	    	this.showlistadmin = true;
		}
  }

  public handleAddressChange(address) {
    // Do some stuff
    this.newaddress = address.formatted_address;
  }

  onReactiveFormSubmit(){
		this.submitted = true;
		if (this.regularForm.invalid) {
            return;
        }
		if(!this.showlistadmin){
			this.listadmin = 3; // users groupid
		}
    let userDetail = {
      username: this.regularForm.controls['username'].value,
      password: this.regularForm.controls['password'].value,
      firstName: this.regularForm.controls['firstName'].value,
      lastName: this.regularForm.controls['lastName'].value,
      email: this.regularForm.controls['email'].value,
      dob: this.regularForm.controls['dob'].value,
      genderRadios: this.regularForm.controls['genderRadios'].value,
      address: this.newaddress,
      phone: this.regularForm.controls['phone'].value,
      profileImage: '',
      sublist: this.regularForm.controls['sublist'].value,
      type: '',
      role: this.regularForm.controls['role'].value, //this.listadmin,
      organization: this.regularForm.controls['organization'].value
    }
		this.userservice.addUser(userDetail).then(data => {
			if(data['status'] == "success"){
				this.router.navigate(['/users/users']);
			}
			else if(data['status'] == "Failed"){

			}
			else if(data['status'] == "User Exits"){
				if(data['value'] == "Username Already Exits."){
					this.usernameerror = true;
					this.emailerror = false;
				}
				else{
					this.emailerror = true;
					this.usernameerror = false;
				}
			}
		});
	}

  change(e){
  	if(e.target.checked){
  		this.listadmin = 4; // list admin groupid
  	}
  	else{
  		this.listadmin = 3; // users groupid
  	}
  }

  cancelEdit(){
		this.regularForm.reset();
	}

}
