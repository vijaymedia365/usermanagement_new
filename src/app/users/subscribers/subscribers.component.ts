import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";

import { UserService } from '../../shared/data/user.service';
import { ListsService } from '../../shared/data/lists.service';

@Component({
  selector: 'app-subscribers',
  templateUrl: './subscribers.component.html',
  styleUrls: ['./subscribers.component.scss']
})
export class SubscribersComponent implements OnInit {
  users = [];
  isListAdmin = false;
  listDetails;
  p: number = 1;
  pageSize: number = 5;
  collectionSize;

  	constructor(public userservice: UserService, private listsservice: ListsService, private router: Router, private route: ActivatedRoute) {
      if(localStorage.getItem('isListAdmin') == 'true'){
          this.isListAdmin = true;
      }
    }

	ngOnInit() {
    this.listsservice.getlist().then(data => {
          console.log(data['lists']);
          this.listDetails = data['lists'];
      },
      error => {
      });
      if(this.isListAdmin){
          let listId = JSON.parse(localStorage.getItem('currentUser'));
          this.userservice.getUsersSubscriptionList(listId['admin_listid']).then(data => {
              console.log(data['users']);
              this.users = data['users'];
              this.collectionSize = data['users'].length;
          });
      }
      else{
          this.userservice.getUsersSubscribtion().then(data => {
              console.log(data['users']);
              this.users = data['users'];
              this.collectionSize = data['users'].length;
          },
          error => {
          });
      }
	}

  selectUser(value){
      this.p = 1;
      if(value != ''){
          this.userservice.getUsersSubscriptionList(value).then(data => {
              console.log(data['users']);
              this.users = data['users'];
              if(data['users']){
                this.collectionSize = data['users'].length;
              }
              else{
                this.collectionSize = 0;
              }
          });
      }
      else{
          this.userservice.getUsersSubscribtion().then(data => {
              console.log(data['users']);
              this.users = data['users'];
              this.collectionSize = data['users'].length;
          },
          error => {
          });
      }
  }

}
